import { createApp } from 'vue'
import './style.css'
import Space from './components/Space.vue'

createApp(Space).mount('#app')
